# (ASE 2020) Name o' Matic

This is a homework project for Advanced Software Engineering 2020.

![screenshot](./screenshot.png)

## How to run locally?

Simply open up the `public/index.html` file in your browser.
As you make changes locally, be sure to refresh the page in your browser.

## Homework

### Background

Currently the Name o' Matic generates a completely random name from the list every time the button
is pressed. This means it's possible for the same name to appear more than once, before other names have been
picked.

For example, with the names:

```
Paul
Theo
Natalie
```

The Name o' Matic could show `Paul, Paul, Theo, Paul` before ever getting to `Natalie`.

### Instructions

Update the Name o' Matic so that it internally shuffles the list of names. Hitting the button just
returns the next item in the shuffle.

This **should not** update the text area. The shuffled list is generated and persisted in the code and
hidden from the user.

### User Scenarios

Given 5 unique names,  
When the user hits the button,  
Then the Name o' Matic will show a random name,  
And for 4 more button presses, it will not show the same name twice.

Given 5 unique names,  
When the user hits the button for the 6th time,  
Then the Name o' Matic will show the name it showed the first time.

### Hint

You'll probably want to use this function:

```javascript
/**
 * Shuffles array in place.
 *
 * Lovingly taken from https://stackoverflow.com/a/6274381/1708147
 *
 * @param {Array} a items An array containing the items.
 */
function shuffle(a) {
  var j, x, i;
  for (i = a.length - 1; i > 0; i--) {
    j = Math.floor(Math.random() * (i + 1));
    x = a[i];
    a[i] = a[j];
    a[j] = x;
  }
  return a;
}
```
